package main

import (
	"codeberg.org/seizeheures/smtporter"
	"codeberg.org/seizeheures/smtporter/internal/db"
	"codeberg.org/seizeheures/smtporter/internal/dispatcher"
	"codeberg.org/seizeheures/smtporter/internal/handler"
	"codeberg.org/seizeheures/smtporter/internal/web"
	"github.com/charmbracelet/log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
)

const (
	envSmtpListenPort   = "SMTP_LISTEN_PORT"
	envSmtpAuthPassword = "SMTP_AUTH_PASSWORD"
	envWebListenPort    = "WEB_LISTEN_PORT"
	envWebAuthPassword  = "WEB_AUTH_PASSWORD"
	envDatabasePath     = "DB_PATH"
	envBaseURL          = "BASE_URL"
	envEnableFailover   = "ENABLE_FAILOVER"
	envEnableWebhook    = "ENABLE_WEBHOOK_LISTENER"
)

var c smtporter.Config

func main() {
	c = smtporter.Config{
		SMTPListenPort:   1025,
		SMTPAuthPassword: "",
		WebListenPort:    8080,
		WebAuthPassword:  "",
		DatabasePath:     "porter.db",
		BaseURL:          "localhost:8080",
		Failover:         false,
		WebhookListener:  false,
	}

	smtpListenPortString, ok := os.LookupEnv(envSmtpListenPort)
	if ok {
		port, err := strconv.ParseUint(smtpListenPortString, 10, 32)
		if err != nil {
			log.Fatalf("could not parse env %s", envSmtpListenPort)
		}
		c.SMTPListenPort = uint32(port)
	}

	smtpAuthPasswordString, ok := os.LookupEnv(envSmtpAuthPassword)
	if ok {
		c.SMTPAuthPassword = smtpAuthPasswordString
	}
	if !ok || smtpAuthPasswordString == "" {
		log.Warnf("%s not defined, SMTP server will run without authentication", envSmtpAuthPassword)
	}

	webListenPortString, ok := os.LookupEnv(envWebListenPort)
	if ok {
		port, err := strconv.ParseUint(webListenPortString, 10, 32)
		if err != nil {
			log.Fatalf("could not parse env %s", envWebListenPort)
		}
		c.WebListenPort = uint32(port)
	}

	webAuthPasswordString, ok := os.LookupEnv(envWebAuthPassword)
	if ok {
		c.WebAuthPassword = webAuthPasswordString
	}
	if !ok || webAuthPasswordString == "" {
		log.Fatalf("no password specified for web interface access, please define env %s", envWebAuthPassword)
	}

	databasePathString, ok := os.LookupEnv(envDatabasePath)
	if ok {
		c.DatabasePath = databasePathString
	}

	baseURLString, ok := os.LookupEnv(envBaseURL)
	if ok {
		c.BaseURL = baseURLString
	}

	enableFailoverString, ok := os.LookupEnv(envEnableFailover)
	if ok {
		failoverEnabled, err := strconv.ParseBool(enableFailoverString)
		if err != nil {
			log.Fatalf("could not parse env %s", envWebListenPort)
		}
		c.Failover = failoverEnabled
	}

	enableWebhookListenerString, ok := os.LookupEnv(envEnableWebhook)
	if ok {
		webhookListenerEnabled, err := strconv.ParseBool(enableWebhookListenerString)
		if err != nil {
			log.Fatalf("could not parse env %s", envEnableFailover)
		}
		c.WebhookListener = webhookListenerEnabled
	}

	db.SetPath(c.DatabasePath)
	go handler.Handle(c)
	go web.Serve(c)

	if c.WebhookListener {
		go dispatcher.ResendLoop()
	}

	tc := make(chan os.Signal)
	signal.Notify(tc, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-tc
		log.Info("Received SIGTERM, exiting")
		os.Exit(0)
	}()

	select {}
}
