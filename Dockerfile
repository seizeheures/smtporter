FROM golang:1.21-alpine as builder

WORKDIR /

COPY . .

ENV CGO_ENABLED 1

RUN ["apk", "add", "build-base"]
RUN ["go", "build", "cmd/smtporter.go"]

FROM alpine:3.18 as smtporter

# --- labels ---
LABEL org.opencontainers.image.title="SMTPorter"
LABEL org.opencontainers.image.authors="Gabriel Bugeaud <gabriel@seizeheures.fr>"
# --- end labels ---

COPY --from=builder /smtporter /smtporter
COPY templates templates
RUN mkdir /plugins

ENV SMTP_LISTEN_PORT 25
ENV WEB_LISTEN_PORT 80
ENV WEB_AUTH_PASSWORD ""
ENV DB_PATH "porter.db"
ENV BASE_URL "localhost:8080"
ENV ENABLE_FAILOVER false
ENV ENABLE_WEBHOOK_LISTENER false

CMD /smtporter

FROM golang:1.21-alpine as plugin-builder

WORKDIR /
COPY plugins .

ENV CGO_ENABLED 1

RUN ["apk", "add", "build-base"]
RUN ["go", "build", "-buildmode=plugin", "-o", "smtporter_brevo_plugin.so", "brevo/brevo_plugin.go"]
RUN ["go", "build", "-buildmode=plugin", "-o", "smtporter_sendgrid_plugin.so", "sendgrid/sendgrid_plugin.go"]

FROM smtporter as smtporter-with-base-plugins

COPY --from=plugin-builder smtporter_brevo_plugin.so /plugins/smtporter_brevo_plugin.so
COPY --from=plugin-builder smtporter_sendgrid_plugin.so /plugins/smtporter_sendgrid_plugin.so
