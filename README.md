# SMTPorter

[![Go Report Card](https://goreportcard.com/badge/codeberg.org/seizeheures/smtporter)](https://goreportcard.com/report/codeberg.org/seizeheures/smtporter) ![Woodpecker CI](https://ci.codeberg.org/api/badges/12720/status.svg)

SMTPorter is a configurable SMTP proxy that aims to allow easy routing of e-mails to different providers.
It is also capable of handling errors by using other configured providers as a fallback.

A minimalist web UI is provided for managing e-mail providers.

> SMTPorter is still under active development.
> 
> Some features may be missing or not function exactly as expected.
> As such, use in production is to be done at your own risk. Read more at [caveats & limitations](#caveats-and-limitations).

## Usage

You can use SMTPorter in a container via Docker, Podman, Kubernetes, etc. or build it from source.

### Container image

For simplicity, the following example relies on Docker.

If you’re using podman, replace `docker` in the following commands with `podman`. It should work the same as Docker, but I haven’t yet tested this.

```shell
# Using the "latest" tag is a bad practice and is only given as an example
docker run -e WEB_AUTH_PASSWORD='ChangeMe!' quay.io/seizeheures/smtporter:latest
```

To use [the base plugins I provide](plugins/README.md#base-plugins), use the tag `<version>-plugins`.

### From source

Requirements:
- Go 1.21+

To compile from source, clone this repository and compile with:

```shell
go build cmd/smtporter.go
```

To start SMTPorter, run:

```shell
WEB_AUTH_PASSWORD="ChangeMe!" ./smtporter
```

## Environment variables

Some configuration options can be set via the following environment variables:

| Variable                  | Default          | Explanation                                                                                         |
|---------------------------|------------------|-----------------------------------------------------------------------------------------------------|
| `WEB_AUTH_PASSWORD`       | N/A              | Authentication password for the web interface. **This parameter is required!**                      |
| `WEB_LISTEN_PORT`         | `8080`           | Port on which to run the web server                                                                 |
| `SMTP_LISTEN_PORT`        | `1025`           | Port on which to run the SMTP server                                                                |
| `SMTP_AUTH_PASSWORD`      | N/A              | Optional authentication password for the SMTP server, to be used with AUTH PLAIN by clients.        |
| `BASE_URL`                | `localhost:8080` | Web interface base URL, used for URL generation                                                     |
| `ENABLE_FAILOVER`         | `false`          | Whether or not to use other configured providers as fallback if sending via selected provider fails |
| `ENABLE_WEBHOOK_LISTENER` | `false`          | Enable webhook functionality. See [plugins](plugins/README.md) for more information.                |

## What's up with the name?

SMTPorter is named after *porters*, people who carry cargo to remote locations.

## Caveats and limitations

The following is a list of caveats and limitations of SMTPorter, most of which are meant to be fixed or improved upon before `1.0.0`.

- Providers cannot be deleted via the web interface.
- Multiple providers cannot be selected to be used in a round-robin strategy.
- HTML template files are not currently embedded in the binary.
This isn’t a problem if using the container image, but if building from source you will need the `templates/` folder in the same directory as the binary.
- SMTPorter has not yet been benchmarked.

## License

SMTPorter is available as open source under the terms of the [AGPLv3 License](https://opensource.org/license/agpl-v3/).
