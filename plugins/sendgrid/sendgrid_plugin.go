package main

import (
	"encoding/json"
)

type sendgridPlugin string

type SendgridEvent struct {
	MessageID string `json:"smtp-id"`
	EventType string `json:"event"`
}

func (p sendgridPlugin) GetMessageIDs(webhookPayload []byte) (messageIDs []string) {
	// sendgrid’s webhooks aren’t sent per event, they often batch multiple events
	// in the same payload, so we have to parse all of them separately
	var events []SendgridEvent
	err := json.Unmarshal(webhookPayload, &events)
	if err != nil {
		// if an error occurs, silently return an empty slice
		return []string{}
	}

	for _, event := range events {
		if event.EventType == "delivered" {
			messageIDs = append(messageIDs, event.MessageID)
		}
	}

	return messageIDs
}

var WebhookPlugin sendgridPlugin
