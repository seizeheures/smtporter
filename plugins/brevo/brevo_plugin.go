package main

import "encoding/json"

type brevoPlugin string

type BrevoEvent struct {
	MessageID string `json:"message-id"`
}

func (p brevoPlugin) GetMessageIDs(webhookPayload []byte) []string {
	var event BrevoEvent
	err := json.Unmarshal(webhookPayload, &event)
	if err != nil {
		// if an error occurs, silently return an empty slice
		return []string{}
	}

	return []string{event.MessageID}
}

var WebhookPlugin brevoPlugin
