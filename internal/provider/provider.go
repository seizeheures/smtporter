package provider

import "encoding/json"
import "github.com/charmbracelet/log"

type Provider struct {
	Name              string `json:"name"`
	Enabled           bool   `json:"enabled"`
	Addr              string `json:"addr"`
	AuthEnabled       bool   `json:"authEnabled"`
	Host              string `json:"host"`
	Username          string `json:"username"`
	Password          string `json:"password"`
	WebhookEnabled    bool   `json:"webhookEnabled"`
	WebhookPluginName string `json:"webhookPluginName"`
}

func LoadFromJSON(data []byte) (Provider, error) {
	p := Provider{}

	err := json.Unmarshal(data, &p)
	if err != nil {
		log.Error(err)
	}

	return p, err
}

func (p *Provider) ToBytes() []byte {
	j, _ := json.Marshal(p)

	return j
}
