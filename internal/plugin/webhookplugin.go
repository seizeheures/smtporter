package plugin

import (
	"fmt"
	"github.com/charmbracelet/log"
	"plugin"
)

type WebhookPlugin interface {
	GetMessageIDs(webhookPayload []byte) []string
}

func MustLoadPlugin(name string) WebhookPlugin {
	p, err := plugin.Open(fmt.Sprintf("plugins/%s", name))
	if err != nil {
		log.Fatal(err)
	}

	wp, err := p.Lookup("WebhookPlugin")
	if err != nil {
		log.Fatal(err)
	}

	var webhookPlugin WebhookPlugin
	webhookPlugin, ok := wp.(WebhookPlugin)
	if !ok {
		log.Fatal("Unexpected type from module symbol")
	}

	return webhookPlugin
}
