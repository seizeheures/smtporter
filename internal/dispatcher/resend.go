package dispatcher

import (
	"codeberg.org/seizeheures/smtporter/internal/message"
	"codeberg.org/seizeheures/smtporter/internal/settings"
	"github.com/charmbracelet/log"
	"slices"
	"sync"
	"time"
)

var resendLoopRunning sync.Mutex

func ResendLoop() {
	if !resendLoopRunning.TryLock() {
		return
	}

	messageGracePeriod, _ := time.ParseDuration("60s")

	for {
		time.Sleep(45 * time.Second)
		keys := message.GetMessageKeys()
		for _, id := range keys {
			go reprocessMessage(id, messageGracePeriod)
		}
	}
}

func reprocessMessage(id string, gracePeriod time.Duration) {
	m, ok := message.FetchMessage(id)
	if !ok {
		return
	}

	now := time.Now()
	targetTime := m.LastSentAt.Add(gracePeriod)

	if now.Before(targetTime) {
		log.Infof("message <%s> - too soon, retrying later", id)
		return
	}

	log.Infof("message <%s> - retrying", id)
	s := settings.Create()
	for name, provider := range s.Providers {
		if !slices.Contains(m.ProvidersTried, name) && provider.Enabled {
			err := DispatchMessageWithProvider(m, provider)
			if err != nil {
				log.Error("message <%s> - error retrying with provider '%s': %s", id, provider, err.Error())
			}
			return
		}
	}
	log.Warnf("message <%s> - already retried with all providers", id)
}
