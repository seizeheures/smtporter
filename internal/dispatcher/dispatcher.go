package dispatcher

import (
	"codeberg.org/seizeheures/smtporter/internal/message"
	"codeberg.org/seizeheures/smtporter/internal/provider"
	"codeberg.org/seizeheures/smtporter/internal/settings"
	"errors"
	"fmt"
	"github.com/charmbracelet/log"
	"net/smtp"
	"slices"
	"time"
)

var providerEvents = false

func DispatchMessageWithProvider(m message.Message, p provider.Provider) error {
	if slices.Contains(m.ProvidersTried, p.Name) {
		return errors.New("cannot retry same provider")
	}

	m.ProvidersTried = append(m.ProvidersTried, p.Name)
	m.LastSentAt = time.Now()

	err := m.Save()
	if err != nil {
		return err
	}

	err = sendMessage(m.From, m.To, m.Data, p)
	if err != nil {
		return err
	}

	if providerEvents {
		m.ProvidersTried = append(m.ProvidersTried, p.Name)
		m.Save()
	}

	return nil
}

func DispatchMessage(m message.Message) error {
	s := settings.Create()
	if s.SelectedProviderName == "" {
		s.SelectedProviderName = "default"
		logError("no provider selected in settings. this should never happen!")
	}

	p := s.Providers[s.SelectedProviderName]
	m.LastSentAt = time.Now()

	err := sendMessage(m.From, m.To, m.Data, p)
	if err != nil {
		logError(err.Error())
		return err
	}

	if providerEvents {
		m.ProvidersTried = append(m.ProvidersTried, p.Name)
		m.Save()
	}

	return nil
}

func DispatchMessageWithFailover(m message.Message) error {
	s := settings.Create()
	if s.SelectedProviderName == "" {
		s.SelectedProviderName = "default"
		logError("no provider selected in settings. this should never happen!")
	}

	p := s.Providers[s.SelectedProviderName]

	var failoverErr error
	sendAttempts := 1

	usedProviderName := p.Name

	// send with configured provider
	err := sendMessage(m.From, m.To, m.Data, p)

	if err != nil {
		log.Infof("dispatcher - sending with configured provider %s (%s) failed", p.Name, p.Addr)
		for name, p := range s.Providers {
			if name == usedProviderName {
				continue
			}
			if !p.Enabled {
				continue
			}

			log.Infof("dispatcher - attempting to failover via provider %s (%s)", p.Name, p.Addr)
			sendAttempts++
			failoverErr = sendMessage(m.From, m.To, m.Data, p)
			if failoverErr == nil {
				usedProviderName = p.Name
				log.Infof("dispatcher - failover #%d succeeded via provider %s (%s)", sendAttempts-1, p.Name, p.Addr)
				break
			}
		}
	}

	if failoverErr != nil {
		logError(fmt.Sprintf("failed to send message after %d attempts over all providers", sendAttempts))
		return failoverErr
	}

	if err == nil && failoverErr == nil {
		if providerEvents {
			m.ProvidersTried = append(m.ProvidersTried, usedProviderName)
			m.LastSentAt = time.Now()
			m.Save()
		}

		return nil
	}

	return err
}

func EnableProviderEvents() {
	providerEvents = true
}

func sendMessage(from string, to []string, data []byte, p provider.Provider) error {
	var auth smtp.Auth

	if p.AuthEnabled {
		auth = smtp.PlainAuth(
			"",
			p.Username,
			p.Password,
			p.Host,
		)
	}

	err := smtp.SendMail(
		p.Addr,
		auth,
		from,
		to,
		data,
	)
	if err != nil {
		logError(err.Error())
	}

	return err
}

func logError(message string) {
	log.Errorf("dispatcher - %s", message)
}
