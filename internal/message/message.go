package message

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/charmbracelet/log"
	"github.com/oklog/ulid"
	"math/rand"
	"strings"
	"time"
)

type Message struct {
	ID             string    `json:"messageId"`
	From           string    `json:"from"`
	To             []string  `json:"to"`
	Data           []byte    `json:"data"`
	ProvidersTried []string  `json:"providersTried"`
	LastSentAt     time.Time `json:"lastSentAt"`
}

func (m *Message) ToBytes() []byte {
	j, _ := json.Marshal(m)

	return j
}

func (m *Message) Save() error {
	return saveMessage(*m)
}

func LoadFromJSON(data []byte) (Message, error) {
	m := Message{}

	err := json.Unmarshal(data, &m)
	if err != nil {
		log.Error(err)
	}

	return m, err
}

func CreateFromRaw(from string, to []string, data []byte) (Message, bool) {
	entropy := rand.New(rand.NewSource(time.Now().UnixNano()))
	ms := ulid.Timestamp(time.Now())
	uid, err := ulid.New(ms, entropy)
	if err != nil {
		log.Error(err)
		return Message{}, false
	}

	id := fmt.Sprintf("%s@smtporter", uid.String())

	data = writeHeader(data, "Message-ID", id)

	m := Message{
		ID:             id,
		From:           from,
		To:             to,
		Data:           data,
		ProvidersTried: []string{},
	}

	return m, true
}

func writeHeader(data []byte, k, v string) []byte {
	s := bufio.NewScanner(bytes.NewReader(data))
	buffer := bytes.NewBuffer([]byte{})
	buffer.Write([]byte(fmt.Sprintf("%s: <%s>\n", k, v)))

	reachedBody := false

	// Scan every line from the input data.
	// If the line starts with the header key, discard it. Otherwise, write it to the output buffer.
	//
	// Per RFC 5322, the headers and body of the message are separated by a blank line.
	// If the body is reached, stop looking for the header key and write every remaining line to the buffer.
	// https://datatracker.ietf.org/doc/html/rfc5322#section-2.1
	for s.Scan() {
		l := s.Text()
		if !reachedBody && s.Text() == "" {
			reachedBody = true
		}
		if reachedBody || !strings.HasPrefix(strings.ToLower(l), strings.ToLower(k)) {
			lineBytes := []byte(l + "\n")
			buffer.Write(lineBytes)
		}
	}

	return buffer.Bytes()
}
