package message

import (
	"codeberg.org/seizeheures/smtporter/internal/db"
	"github.com/charmbracelet/log"
)

const messagesBucketName = "messages"

var storageInitialized = false

func initStorage() {
	db.Initialize([]string{messagesBucketName})
	storageInitialized = true
}

func FetchMessage(ID string) (Message, bool) {
	if storageInitialized == false {
		initStorage()
	}
	mb := db.Read(messagesBucketName, ID)

	if mb == nil {
		return Message{}, false
	}

	m, err := LoadFromJSON(mb)
	if err != nil {
		log.Error(err)
	}

	return m, true
}

func GetMessageKeys() (keys []string) {
	if storageInitialized == false {
		initStorage()
	}

	keys, err := db.GetKeys(messagesBucketName)
	if err != nil {
		log.Error(err)
	}

	return keys
}

func saveMessage(message Message) error {
	return db.Write(messagesBucketName, message.ID, message.ToBytes())
}

func DeleteMessage(message Message) error {
	return db.Remove(messagesBucketName, message.ID)
}
