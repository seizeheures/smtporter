package handler

import (
	"codeberg.org/seizeheures/smtporter"
	"codeberg.org/seizeheures/smtporter/internal/dispatcher"
	"codeberg.org/seizeheures/smtporter/internal/message"
	"fmt"
	"github.com/charmbracelet/log"
	"github.com/mhale/smtpd"
	"net"
)

var failoverEnabled = false
var authPassword = ""

func mailHandler(origin net.Addr, from string, to []string, data []byte) error {
	m, ok := message.CreateFromRaw(from, to, data)
	if !ok {
		log.Error("could not parse message")
	}

	if failoverEnabled {
		return dispatcher.DispatchMessageWithFailover(m)
	}

	return dispatcher.DispatchMessage(m)
}

func authHandler(remoteAddr net.Addr, mechanism string, username, password, shared []byte) (bool, error) {
	return string(password) == authPassword, nil
}

func Handle(c smtporter.Config) {
	log.Infof("starting SMTP server on port %d", c.SMTPListenPort)
	if c.Failover {
		failoverEnabled = true
		log.Info("provider failover enabled")
	}
	if c.WebhookListener {
		dispatcher.EnableProviderEvents()
		log.Info("provider events enabled")
	}

	authPassword = c.SMTPAuthPassword

	srv := &smtpd.Server{
		Addr:         fmt.Sprintf(":%d", c.SMTPListenPort),
		Appname:      "smtporter",
		AuthHandler:  nil,
		AuthMechs:    nil,
		AuthRequired: false,
		Handler:      mailHandler,
		Hostname:     "",
	}

	if authPassword != "" {
		srv.AuthRequired = true
		srv.AuthMechs = map[string]bool{"PLAIN": true}
		srv.AuthHandler = authHandler
	}

	srv.ListenAndServe()
}
