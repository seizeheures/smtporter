package handlers

import (
	"codeberg.org/seizeheures/smtporter/internal/settings"
	"codeberg.org/seizeheures/smtporter/internal/web/templates"
	"github.com/charmbracelet/log"
	"net/http"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	s := settings.Create()

	c, err := r.Cookie("saved")
	if err != nil {
		log.Error(err)
	}

	if c != nil {
		http.SetCookie(w, &http.Cookie{Name: "saved", Value: "", MaxAge: 0})
	}

	templates.Render(w, "templates/index.html", struct {
		Settings settings.Settings
		Saved    bool
		BaseURL  string
	}{
		Settings: s,
		Saved:    c != nil && c.Value == "true",
		BaseURL:  baseURL,
	})
}
