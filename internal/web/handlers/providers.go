package handlers

import (
	"codeberg.org/seizeheures/smtporter/internal/provider"
	"codeberg.org/seizeheures/smtporter/internal/settings"
	"codeberg.org/seizeheures/smtporter/internal/web/templates"
	"fmt"
	"github.com/charmbracelet/log"
	"github.com/go-chi/chi/v5"
	"net/http"
)

func EditProviderHandler(w http.ResponseWriter, r *http.Request) {
	s := settings.Create()

	name := chi.URLParam(r, "name")

	p, ok := s.Providers[name]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	c, err := r.Cookie("saved")
	if err != nil {
		log.Error(err)
	}

	if c != nil {
		http.SetCookie(w, &http.Cookie{Name: "saved", Value: "", MaxAge: 0})
	}

	templates.Render(w, "templates/edit_provider.html", struct {
		Provider provider.Provider
		Saved    bool
	}{
		Provider: p,
		Saved:    c != nil && c.Value == "true",
	})
}

func CreateProviderHandler(w http.ResponseWriter, r *http.Request) {
	templates.Render(w, "templates/create_provider.html", struct {
		BaseURL string
	}{
		BaseURL: baseURL,
	})
}

func SaveNewProvider(w http.ResponseWriter, r *http.Request) {
	s := settings.Create()

	p := provider.Provider{
		Name:              r.PostFormValue("name"),
		Enabled:           r.PostFormValue("enabled") == "on",
		Addr:              r.PostFormValue("addr"),
		AuthEnabled:       r.PostFormValue("auth") == "on",
		Host:              r.PostFormValue("host"),
		Username:          r.PostFormValue("username"),
		Password:          r.PostFormValue("password"),
		WebhookEnabled:    r.PostFormValue("webhook") == "on",
		WebhookPluginName: r.PostFormValue("webhook-plugin-name"),
	}

	s.SaveProvider(p)

	http.SetCookie(w, &http.Cookie{Name: "saved", Value: "true"})
	http.Redirect(w, r, fmt.Sprintf("%s/provider/%s", baseURL, p.Name), http.StatusFound)
}

func UseProviderHandler(w http.ResponseWriter, r *http.Request) {
	s := settings.Create()

	name := r.PostFormValue("provider")

	log.Info(name)

	_, ok := s.Providers[name]
	if !ok {
		http.Redirect(w, r, fmt.Sprintf("%s/", baseURL), http.StatusFound)
		return
	}

	s.SelectedProviderName = name

	s.Save()
	http.SetCookie(w, &http.Cookie{Name: "saved", Value: "true"})
	http.Redirect(w, r, fmt.Sprintf("%s/", baseURL), http.StatusFound)
}

func SaveProviderHandler(w http.ResponseWriter, r *http.Request) {
	s := settings.Create()

	name := chi.URLParam(r, "name")

	p, ok := s.Providers[name]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	p.AuthEnabled = r.PostFormValue("auth") == "on"
	p.Enabled = r.PostFormValue("enabled") == "on"
	p.Addr = r.PostFormValue("addr")
	p.Host = r.PostFormValue("host")
	p.Username = r.PostFormValue("username")
	p.Password = r.PostFormValue("password")
	p.WebhookEnabled = r.PostFormValue("webhook") == "on"
	p.WebhookPluginName = r.PostFormValue("webhook-plugin-name")

	s.SaveProvider(p)

	http.SetCookie(w, &http.Cookie{Name: "saved", Value: "true"})
	http.Redirect(w, r, fmt.Sprintf("%s/provider/%s", baseURL, p.Name), http.StatusFound)
}
