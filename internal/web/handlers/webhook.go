package handlers

import (
	"codeberg.org/seizeheures/smtporter/internal/message"
	"codeberg.org/seizeheures/smtporter/internal/plugin"
	"codeberg.org/seizeheures/smtporter/internal/settings"
	"github.com/charmbracelet/log"
	"github.com/go-chi/chi/v5"
	"io"
	"net/http"
	"strings"
)

type Path struct {
	Node Node
}

type Node struct {
	Name   string
	Child  *Node
	Parent *Node
}

func WebhookHandler(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "provider-name")
	s := settings.Create()

	p, ok := s.Providers[name]

	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	if !p.WebhookEnabled {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	b, _ := io.ReadAll(r.Body)

	webhookPlugin := plugin.MustLoadPlugin(p.WebhookPluginName)

	for _, mid := range webhookPlugin.GetMessageIDs(b) {
		mid = strings.ReplaceAll(mid, "<", "")
		mid = strings.ReplaceAll(mid, ">", "")

		m, ok := message.FetchMessage(mid)
		if !ok {
			continue
		}

		err := message.DeleteMessage(m)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		log.Infof("message <%s> - delivered (heard back from provider '%s')", mid, name)
	}

	w.WriteHeader(http.StatusOK)
}
