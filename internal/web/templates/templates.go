package templates

import (
	"github.com/charmbracelet/log"
	"html/template"
	"net/http"
)

func Render(w http.ResponseWriter, tmpl string, data any) {
	t, _ := template.ParseFiles(tmpl)
	err := t.Execute(w, data)
	if err != nil {
		log.Fatal(err)
	}
}
