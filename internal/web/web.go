package web

import (
	"codeberg.org/seizeheures/smtporter"
	"codeberg.org/seizeheures/smtporter/internal/web/handlers"
	"fmt"
	"github.com/charmbracelet/log"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"net/http"
)

func Serve(c smtporter.Config) {
	authMap := map[string]string{"": c.WebAuthPassword}

	handlers.SetBaseURL(c.BaseURL)
	r := chi.NewRouter()
	r.Use(middleware.StripSlashes)

	r.With(middleware.BasicAuth("smtporter", authMap)).Get("/", handlers.IndexHandler)
	r.With(middleware.BasicAuth("smtporter", authMap)).Route("/provider", func(r chi.Router) {
		r.Post("/{name}", handlers.SaveProviderHandler)
		r.Get("/{name}", handlers.EditProviderHandler)
	})
	r.With(middleware.BasicAuth("smtporter", authMap)).Get("/create-provider", handlers.CreateProviderHandler)
	r.With(middleware.BasicAuth("smtporter", authMap)).Post("/create-provider", handlers.SaveNewProvider)
	r.With(middleware.BasicAuth("smtporter", authMap)).Post("/use-provider", handlers.UseProviderHandler)

	if c.WebhookListener {
		r.Post("/webhook/{provider-name}", handlers.WebhookHandler)
	}

	log.Infof("starting web interface on port %d", c.WebListenPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", c.WebListenPort), r))
}
