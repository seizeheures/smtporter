package settings

import (
	"codeberg.org/seizeheures/smtporter/internal/db"
	"codeberg.org/seizeheures/smtporter/internal/provider"
	"errors"
	"github.com/charmbracelet/log"
	"strconv"
	"time"
)

const (
	settingsBucketName      = "settings"
	settingsProviderNameKey = "selectedProviderName"
	settingsSavedAtKey      = "savedAt"
	providersBucketName     = "providers"
	defaultProviderName     = "default"
)

var ErrCannotDeleteLastProvider = errors.New("cannot delete last provider. there must be at least one provider present in config")
var ErrCannotDeleteSelectedProvider = errors.New("cannot delete currently selected provider")

type Settings struct {
	SavedAt              int64
	Providers            map[string]provider.Provider
	SelectedProviderName string
}

func (s *Settings) SetProvider(p provider.Provider) {
	_, ok := s.Providers[p.Name]
	if !ok {
		s.SaveProvider(p)
	}

	s.SelectedProviderName = p.Name

	s.Save()
}

func (s *Settings) RemoveProvider(p provider.Provider) error {
	if p.Name == s.SelectedProviderName {
		return ErrCannotDeleteSelectedProvider
	}
	// don’t allow removing all providers
	if len(s.Providers) <= 1 {
		return ErrCannotDeleteLastProvider
	}
	// if s.Providers[name] does not exist, this is a no-op
	// therefore it is safe to just call delete() without checking beforehand
	delete(s.Providers, p.Name)

	s.Save()

	return nil
}

func (s *Settings) SaveProvider(p provider.Provider) {
	s.Providers[p.Name] = p

	s.Save()
}

func (s *Settings) Save() {
	log.Info("Saving settings")
	s.SavedAt = time.Now().Unix()
	providers := map[string][]byte{}
	db.Write(settingsBucketName, settingsProviderNameKey, []byte(s.SelectedProviderName))

	for _, p := range s.Providers {
		providers[p.Name] = p.ToBytes()
	}

	keys, err := db.GetKeys(providersBucketName)
	if err != nil {
		log.Fatal(err)
	}

	for _, k := range keys {
		_, ok := providers[k]
		if !ok {
			err := db.Remove(providersBucketName, k)
			if err != nil {
				log.Error(err)
			}
		}
	}

	for k, v := range providers {
		err := db.Write(providersBucketName, k, v)
		if err != nil {
			log.Error(err)
		}
	}

	err = db.Write(settingsBucketName, settingsSavedAtKey, []byte(strconv.FormatInt(s.SavedAt, 10)))
	if err != nil {
		log.Error(err)
	}
	log.Info("Settings saved")
}

func Create() Settings {
	db.Initialize([]string{settingsBucketName, providersBucketName})
	b := db.Read(settingsBucketName, settingsSavedAtKey)

	var s Settings

	s = Settings{
		Providers:            map[string]provider.Provider{},
		SelectedProviderName: "default",
	}

	if b == nil {
		s.SavedAt = time.Now().Unix()
	} else {
		savedAt, err := strconv.ParseInt(string(b), 10, 64)
		if err != nil {
			log.Fatal(err)
		}
		s.SavedAt = savedAt
		s.SelectedProviderName = string(db.Read(settingsBucketName, settingsProviderNameKey))
	}

	providerKeys, err := db.GetKeys(providersBucketName)
	if err != nil {
		log.Error(nil)
	}

	for _, key := range providerKeys {
		data := db.Read(providersBucketName, key)
		p, err := provider.LoadFromJSON(data)

		s.Providers[p.Name] = p

		if err != nil {
			log.Error(err)
		}
	}

	if len(s.Providers) == 0 {
		log.Info("Settings - creating default provider")
		s.Providers = map[string]provider.Provider{
			"default": {
				Name:        defaultProviderName,
				Addr:        "0.0.0.0:25",
				AuthEnabled: false,
				Host:        "",
				Username:    "",
				Password:    "",
			},
		}
	}

	return s
}
