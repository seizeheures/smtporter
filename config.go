package smtporter

type Config struct {
	SMTPListenPort   uint32
	SMTPAuthPassword string
	WebListenPort    uint32
	WebAuthPassword  string
	DatabasePath     string
	BaseURL          string
	Failover         bool
	WebhookListener  bool
}
